# Depends: make sassc sass-stylesheets-compass compass-h5bp-plugin pandoc graphviz git po4a

PROJECT_DESC = Solidbox website
PROJECT_OWNER = Siri Reiter <siri@jones.dk>, Jonas Smedegaard <dr@jones.dk>
PROJECT_EMAIL = partners@couchdesign.org
PROJECT_TAG = $(shell git name-rev --tags --name-only HEAD)
DEFAULTLOCALE = en
EXTRALOCALES := $(patsubst locale/%.po,%,$(wildcard locale/*.po))

markdown-snippets := topbar.mdwn footer.mdwn
markdown := $(filter-out $(markdown-snippets:mdwn=%) $(wildcard *.*.mdwn */*.*.mdwn */*/*.*.mdwn),\
 $(wildcard *.mdwn */*.mdwn */*/*.mdwn))
markdown-l10n = $(foreach locale,$(EXTRALOCALES),\
 $(patsubst %.mdwn,%.$(locale).mdwn,$(markdown-snippets) $(markdown)))
hypertext = $(subst index/index,index,$(markdown:%.mdwn=%/index.$(DEFAULTLOCALE).html))
hypertext-base = $(hypertext:.$(DEFAULTLOCALE).html=.html)
hypertext-l10n = $(foreach locale,$(EXTRALOCALES),\
 $(hypertext:.$(DEFAULTLOCALE).html=.$(locale).html))
graph := $(wildcard *.dot */*.dot */*/*.dot)
vectorimage-graph := $(graph:%.dot=%.svg)

sitetitle := $(shell head --lines=1 index.mdwn \
 | grep --perl-regexp --only-matching '^\#\s*\K(\S+(\s\S+)*)+')

all: $(hypertext) $(hypertext-l10n) $(hypertext-base) $(vectorimage-graph) style.css

update: .git
	git pull
	$(MAKE) all

update-po: locale/content.pot $(wildcard locale/*.po)

$(vectorimage-graph): %.svg: %.dot Makefile
	dot -Tsvg -o$@ $<

define LOCALE_template =
$$(markdown-snippets:mdwn=$1.html): %.$1.html: %$(filter-out .$(DEFAULTLOCALE),.$1).mdwn Makefile
	pandoc -f markdown -t html \
	 -o $$@ $$<
	sed -i \
	 -e '1i <div class=\"$$*\">' \
	 -e '$$$$ a </div>' \
	 $$@

%.$1.mdwn: %.mdwn Makefile
	po4a-translate -M UTF-8 -L UTF-8 -f text -o markdown -k 100 \
	 -m $$< \
	 -p locale/$1.po \
	 -l $$@

%.$1.html %/index.$1.html: %$(filter-out .$(DEFAULTLOCALE),.$1).mdwn style.css topbar.$1.html footer.$1.html Makefile
	$$(eval title = $$(shell head --lines=1 $$< \
	 | grep --perl-regexp --only-matching '^\#\s*\K(\S+(\s\S+)*)+'))
	$$(if $$(filter-out ./,$$(dir $$@)),mkdir -p $$(dir $$@))
	pandoc -f commonmark -t html -s \
	 --metadata lang=$1 \
	 $$(if $$(title),\
	  $$(if $$(filter-out $(sitetitle),$$(title)),$$(strip \
	   --metadata pagetitle='$(sitetitle) -- $$(title)'),$$(strip \
	   --metadata pagetitle='$$(title)'))) \
	 -c $$(shell realpath --relative-to=$$(dir $$@) style.css) \
	 -B topbar.$1.html \
	 -A footer.$1.html \
	 -o $$@ $$< $(if $(filter-out $(DEFAULTLOCALE),$1),|| rm -f $$@)
endef
$(foreach locale,$(DEFAULTLOCALE) $(EXTRALOCALES),\
 $(eval $(call LOCALE_template,$(locale))))

%.po %.pot : $(markdown) $(markdown-snippets)
	po4a-updatepo -M UTF-8 -f text -o markdown \
	 --package-name "$(PROJECT_DESC)" \
	 --copyright-holder "$(PROJECT_OWNER)" \
	 --package-version "$(PROJECT_TAG)" \
	 --msgid-bugs-address "$(PROJECT_EMAIL)" \
	 $(patsubst %,-m %,$^) -p $@

%.html: %.$(DEFAULTLOCALE).html
	ln -sf $(notdir $<) $(patsubst %.$(DEFAULTLOCALE).html,%.html,$@)

%.css : %.scss Makefile
	sassc --load-path /usr/share/sass --style compressed $< $@

clean:
	rm -f $(vectorimage-graph)
	find * -type f,l -name '*.html' -delete
	find * -type d -empty -delete
	rm -f style.css style.css.map
	rm -rf .sass-cache
	find -type f -regextype posix-extended \
	 -regex '.*\.($(subst $() ,|,$(EXTRALOCALES)))\.mdwn' -delete

MAKEFLAGS += --jobs
.NOTPARALLEL: source
.PHONY: all update update-po clean
